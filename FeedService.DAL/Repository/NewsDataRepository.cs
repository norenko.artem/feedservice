﻿using FeedService.DAL.Entity;
using FeedService.DAL.Options;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;

namespace FeedService.DAL.Repository
{
    public class NewsDataRepository<TEntity> :INewsDataRepository<TEntity>
         where TEntity : class, IBaseEntity
    {
        private readonly IDbContextOptionProvider _optionsProvider;
        public NewsDataRepository(IDbContextOptionProvider optionsProvider)
        {
            _optionsProvider = optionsProvider;
        }

        public virtual TEntity Add(TEntity Entity)
        {
            using (var context = new NewsContext(_optionsProvider))
            {
                var result = context.Set<TEntity>().Add(Entity);
                context.SaveChanges();
                return result.Entity;
            }
        }

        public virtual void Add(IEnumerable<TEntity> Entity)
        {
            using (var context = new NewsContext(_optionsProvider))
            {
                foreach (var item in Entity)
                {
                    var result = context.Set<TEntity>().Add(item);
                }
                context.SaveChanges();
            }
        }

        public virtual IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> predicate = null, Func<IQueryable<TEntity>, IQueryable<TEntity>> includeFunction = null, bool asNoTracking = false)
        {
            using (var context = new NewsContext(_optionsProvider))
            {
                //TODO: asNoTracking should be used
                IQueryable<TEntity> query = context.Set<TEntity>();
                if (predicate != null)
                {
                    query = query.Where(predicate);
                }
                if (includeFunction != null)
                {
                    query = includeFunction(query);
                }
                return query.ToList();
            }
        }

        public virtual TEntity GetSingle(Expression<Func<TEntity, bool>> predicate = null, Func<IQueryable<TEntity>, IQueryable<TEntity>> includeFunction = null, bool asNoTracking = false)
        {
            return Get(predicate, includeFunction, asNoTracking).FirstOrDefault();
        }

        public virtual void Remove(int Id)
        {
            using (var context = new NewsContext(_optionsProvider))
            {
                var result = context.Set<TEntity>().Find(Id);
                context.Set<TEntity>().Remove(result);
                context.SaveChanges();
            }
        }

        public virtual void Remove(TEntity Entity)
        {
            using (var context = new NewsContext(_optionsProvider))
            {
                var result = context.Set<TEntity>().Find(Entity);
                context.Set<TEntity>().Remove(result);
                context.SaveChanges();
            }
        }

        public virtual TEntity Update(TEntity Entity)
        {
            using (var context = new NewsContext(_optionsProvider))
            {
                var result = context.Set<TEntity>().Attach(Entity);
                context.SaveChanges();
                return result.Entity;
            }
        }

        public virtual void Update(IEnumerable<TEntity> Entity)
        {
            using (var context = new NewsContext(_optionsProvider))
            {
                foreach (var item in Entity)
                {
                    var result = context.Set<TEntity>().Attach(item);
                }
                context.SaveChanges();
            }
        }
    

}
}
