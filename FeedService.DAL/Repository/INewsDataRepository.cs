﻿using FeedService.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace FeedService.DAL.Repository
{
    public interface INewsDataRepository<TEntity>
        where TEntity : IBaseEntity
    {
        /// <summary>
        /// Add an entity (for example news) into database
        /// </summary>
        /// <param name="Entity">any type of entity we need/use in our app</param>
        /// <returns></returns>
        TEntity Add(TEntity Entity);
        /// <summary>
        /// overload method to add range of entities into database
        /// </summary>
        /// <param name="Entity">any type of entity we need/use in our app</param>
        void Add(IEnumerable<TEntity> Entity);
        /// <summary>
        /// Get list of entities from database using expression if it needs
        /// </summary>
        /// <param name="predicate">Expression for choice data from data base.</param>
        /// <param name="includeFunction">Function for choice list of included properties.</param>
        /// <param name="asNoTracking">Flag indicating if AsNoTracking is turned on or off.</param>
        /// <returns></returns>
        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> predicate = null, Func<IQueryable<TEntity>, IQueryable<TEntity>> includeFunction = null, bool asNoTracking = false);
        /// <summary>
        /// Get an entity from database using expression if it needs
        /// </summary>
        /// <param name="predicate">Expression for choice data from data base.</param>
		/// <param name="includeFunction">Function for choice list of included properties.</param>
		/// <param name="asNoTracking">Flag indicating if AsNoTracking is turned on or off.</param>
        /// <returns></returns>
        TEntity GetSingle(Expression<Func<TEntity, bool>> predicate = null, Func<IQueryable<TEntity>, IQueryable<TEntity>> includeFunction = null, bool asNoTracking = false);
        /// <summary>
        /// Update an entity we have
        /// </summary>
        /// <param name="Entity">any type of entity we need/use in our app</param>
        /// <returns></returns>
        TEntity Update(TEntity Entity);
        /// <summary>
        /// Update list of entities we have
        /// </summary>
        /// <param name="Entity">any type of entity we need/use in our app</param>
        void Update(IEnumerable<TEntity> Entity);
        /// <summary>
        /// remove an entity by id
        /// </summary>
        /// <param name="Id">integer type of entities id</param>
        void Remove(int Id);
        /// <summary>
        /// Remove an entity using entity obj
        /// </summary>
        /// <param name="Entity">any type of entity we need/use in our app</param>
        void Remove(TEntity Entity);

    }
}
