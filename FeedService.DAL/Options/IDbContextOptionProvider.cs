﻿using Microsoft.EntityFrameworkCore;

namespace FeedService.DAL.Options
{
    /// <summary>
	/// Contract for all classes which are providing db context options
	/// </summary>
    public interface IDbContextOptionProvider
    {
        /// <summary>
        /// Returns options for database context
        /// </summary>
        /// <returns></returns>
        DbContextOptions GetOptions();

        /// <summary>
        /// True if migration is allowed
        /// </summary>
        bool AllowMigration { get; }

        /// <summary>
        /// True if default data should be added into db
        /// </summary>
        bool AddDefaultData { get; }
    }
}
