﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;


namespace FeedService.DAL.Options
{
    /// <summary>
	/// Class that provides default db context options
	/// </summary>
    public class DefaultDbContextOption : IDbContextOptionProvider
    {
        #region members
        private readonly IConfigurationRoot _config;
        private DBType _dbType;
        #endregion

        #region constructors
        public DefaultDbContextOption(IConfigurationRoot config)
        {
            _config = config;
            _dbType = DBType.Sql;
        }
        #endregion
        public DbContextOptions GetOptions()
        {
            var builder = new DbContextOptionsBuilder<NewsContext>();
            var connectionString = _config.GetConnectionString("NewsConnection");

            if (_dbType == DBType.Sql)
            {
                builder.UseSqlServer(connectionString);
            }
            else if(_dbType == DBType.InMemmory)
            {
                //builder.UseInMemoryDatabase(connectionString);
                AllowMigration = false;
            }

            builder.EnableSensitiveDataLogging();
            var options = builder.Options;
            return options;
        }
        public bool AllowMigration { get; private set; } = true;

        public bool AddDefaultData { get; } = true;
    }
}
