﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedService.DAL.Options
{
    public enum DBType
    {
        Sql,
        InMemmory,
        SQLite,
        None
    }
}
