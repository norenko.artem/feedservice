﻿namespace FeedService.DAL.Entity
{
    public interface IBaseEntity
    {
        int Id { get; set; }
        string Title { get; set; }
        string Description { get; set; }
    }
}
