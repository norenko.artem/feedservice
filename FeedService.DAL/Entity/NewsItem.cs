﻿using FeedService.DAL.Entity.Base;

namespace FeedService.DAL.Entity
{
    public class NewsItem : BaseEntity
    {
        public string SourceUrl { get; set; }
    }
}
