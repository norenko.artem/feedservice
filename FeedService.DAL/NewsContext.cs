﻿using FeedService.DAL.Entity;
using FeedService.DAL.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace FeedService.DAL
{
    public class NewsContext : DbContext
    {
        public NewsContext(IDbContextOptionProvider options) : base(options.GetOptions())
        {
            
        }

        public NewsContext(DbContextOptions options)
            : base(options)
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }
        public DbSet<NewsItem> NewsItem { get; set; }
        
    }
}
