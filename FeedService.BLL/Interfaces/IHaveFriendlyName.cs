﻿namespace FeedService.BLL.Interfaces
{
    /// <summary>
	/// Contract for models that have friendly names
	/// </summary>
	public interface IHaveFriendlyName
    {
        /// <summary>
        /// Get friendly name for current model
        /// </summary>
        /// <returns>friendly name string</returns>
        string GetFriendlyName();
    }
}
