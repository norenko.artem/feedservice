﻿using FeedService.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FeedService.BLL.Interfaces
{
    public interface ICanIncludeProperties<TEntity>
         where TEntity : IBaseEntity
    {
        /// <summary>
        /// Include navigation properties by specified query
        /// </summary>
        /// <param name="includeQuery">Query to include navigation properties</param>
        /// <returns>Query with included navigation properties</returns>
        IQueryable<TEntity> IncludeProperties(IQueryable<TEntity> includeQuery);
    }
}
