﻿using FeedService.DAL.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FeedService.BLL.Interfaces
{
    public interface IBaseModel : IHaveFriendlyName
    {
        int Id { get; set; }
        string Title { get; set; }
        string Description { get; set; }
        bool Validate(ICollection<ValidationResult> results);
    }
    public interface IBaseModel<TEntity> : ICanIncludeProperties<TEntity>, IBaseModel where TEntity : IBaseEntity
    {
        /// <summary>
        /// Convert entity to model
        /// </summary>
        /// <param name="entity">Entity to be converted</param>
        /// <returns>Converted model</returns>
        IBaseModel FromEntity(TEntity entity);

        /// <summary>
        /// Convert model to Entity
        /// </summary>
        /// <returns>Entity</returns>
        TEntity ToEntity();
    }
}
