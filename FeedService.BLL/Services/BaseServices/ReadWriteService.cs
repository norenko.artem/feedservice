﻿using FeedService.BLL.Interfaces;
using FeedService.BLL.Services.BaseServices.ExtraClasses;
using FeedService.BLL.Services.Interfaces;
using FeedService.DAL.Entity;
using System;
using System.Collections.Generic;
using FeedService.BLL.Models.Configs;
using FeedService.BLL.Validation;
using System.Linq;
using FeedService.DAL.Options;

namespace FeedService.BLL.Services
{
    public abstract class ReadWriteService<TModel, TEntity> : ReadService<TModel, TEntity>, IWriteService<TModel>
        where TModel : class, IBaseModel<TEntity>, new()
        where TEntity : class, IBaseEntity, new()
    {
        protected IModelValidator<TModel> ModelValidator = new ModelValidator<TModel>();

        #region Constructors
        protected ReadWriteService(IDbContextOptionProvider optionsProvider) : base(optionsProvider)
        {

        }
        #endregion

        #region Methods for ovveriding
        protected virtual TEntity ConvertModelToEntity(OperationTypes operation, TModel model)
        {
            return model.ToEntity();
        }

        protected virtual SingleOperationResult<TModel> PerformSingleOperation(OperationTypes operationType, TModel model, Func<TEntity, TEntity> operationFunc)
        {
            var errorMessageByLevel = string.Empty;
            try
            {
                var validationErrors = ModelValidator.Validate(model);
                if (validationErrors.Any())
                {
                    return SingleError(operationType, model, validationErrors);
                }
                errorMessageByLevel = ServerMessages.ModelConversionError;
                var entity = ConvertModelToEntity(operationType, model);
                errorMessageByLevel = ServerMessages.ErrorInDataBase;
                operationFunc(entity);
                errorMessageByLevel = ServerMessages.ErrorWhileRetrieving;
                entity = _NewsDataRepository.GetSingle(i => i.Id == entity.Id, IncludePropertyFunc);
                return SingleSuccess(operationType, entity.ToModel<TModel, TEntity>(), GetSingleOperationMessage);
            }
            catch (Exception ex)
            {
                
                var result = SingleError(operationType, model, GetSingleOperationMessage);
                result.Message += $" {errorMessageByLevel}.";
#if DEBUG
                result.Message += $" Exception: {ex.Message} StackTrace: {ex.StackTrace}";
#endif
                return result;
            }
        }

        protected virtual CollectionOperationResult<TModel> PerformCollectionOperation(OperationTypes operationType, IEnumerable<TModel> models, Action<IEnumerable<TEntity>> operationFunc)
        {
            try
            {
                var validationErrors = models.SelectMany(model => ModelValidator.Validate(model));
                if (validationErrors.Any())
                {
                    return CollectionError(OperationTypes.Add, models, validationErrors);
                }
                var entities = models.Select(model => ConvertModelToEntity(OperationTypes.Add, model));
                operationFunc(entities);
                return CollectionSuccess(OperationTypes.Add, entities.ToModels<TModel, TEntity>(), GetCollectionOperationMessage);
            }
            catch (Exception ex)
            {
               
                return CollectionError(OperationTypes.Add, models, GetCollectionOperationMessage);
            }
        }

        private string GetSingleOperationMessage(OperationTypes operation, TModel model, bool isSuccess = true)
        {
            var message = $"{model.GetFriendlyName()} was {(isSuccess ? "" : "not")}";
            switch (operation)
            {
                case OperationTypes.Add: message = $"{message} added."; break;
                case OperationTypes.Update: message = $"{message} updated."; break;
                case OperationTypes.Delete: message = $"{message} deleted."; break;
            }
            return isSuccess ? message : $"{message} {ServerMessages.TryAgain}.";
        }

        private string GetCollectionOperationMessage(OperationTypes operation, IEnumerable<TModel> models, bool isSuccess = true)
        {
            var message = $"Records was {(isSuccess ? "" : "not")}";
            switch (operation)
            {
                case OperationTypes.Add: message = $"{message} added."; break;
                case OperationTypes.Update: message = $"{message} updated."; break;
                case OperationTypes.Delete: message = $"{message} deleted."; break;
            }
            return isSuccess ? message : $"{message} {ServerMessages.TryAgain}.";
        }

        #endregion
        public SingleOperationResult<TModel> Add(TModel model)
        {
            return PerformSingleOperation(OperationTypes.Add,model,_NewsDataRepository.Add);
        }

        public CollectionOperationResult<TModel> AddRange(IEnumerable<TModel> models)
        {
            return PerformCollectionOperation(OperationTypes.Add, models, _NewsDataRepository.Add);
        }

        public SingleOperationResult<TModel> Remove(int id)
        {
            var entity = _NewsDataRepository.GetSingle(item=>item.Id == id, IncludePropertyFunc).ToModel<TModel, TEntity>();
            try
            {
                var result = SingleSuccess(OperationTypes.Delete, entity, GetSingleOperationMessage);
                _NewsDataRepository.Remove(id);
                return result;
            }
            catch (Exception)
            {
                return SingleSuccess(OperationTypes.Delete, entity, GetSingleOperationMessage);
            }
        }

        public SingleOperationResult<TModel> SaveChanges(TModel model, OperationTypes operation)
        {
            try
            {
                switch (operation)
                {
                    case OperationTypes.Add:
                        return Add(model);
                    case OperationTypes.Update:
                        return Update(model);
                    case OperationTypes.Delete:
                        return Remove(model.Id);
                    default:
                        return SingleError(operation, ServerMessages.UnsupportedOperation, model);
                }
            }
            catch (Exception)
            {
                return SingleError(operation, model);
            }
        }

        public IEnumerable<SingleOperationResult<TModel>> SaveChanges(IEnumerable<TModel> models, OperationTypes operation)
        {
            return models.Select(m => SaveChanges(m, operation));
        }

        public SingleOperationResult<TModel> Update(TModel model)
        {
            return PerformSingleOperation(OperationTypes.Update, model, _NewsDataRepository.Update);
        }

        public CollectionOperationResult<TModel> UpdateRange(IEnumerable<TModel> models)
        {
            return PerformCollectionOperation(OperationTypes.Update, models, _NewsDataRepository.Update);
        }

    }
}
