﻿using System;
using FeedService.BLL.Interfaces;
using FeedService.BLL.Services;
using FeedService.BLL.Services.BaseServices.ExtraClasses;
using FeedService.BLL.Services.Interfaces;
using FeedService.BLL.Models.Configs;
using FeedService.DAL.Entity;
using FeedService.DAL.Repository;
using System.Linq;
using System.Linq.Expressions;
using FeedService.DAL;
using FeedService.DAL.Options;

namespace FeedService.BLL
{
    public abstract class ReadService<TModel, TEntity> : DataService<TModel, TEntity>, IReadService<TModel, TEntity>
        where TModel : class, IBaseModel<TEntity>, new()
        where TEntity : class, IBaseEntity, new()
    {

        protected readonly Func<IQueryable<TEntity>, IQueryable<TEntity>> IncludePropertyFunc;
        #region Constructor 
        
        protected ReadService(IDbContextOptionProvider optionsProvider) : this(new NewsDataRepository<TEntity>(optionsProvider))
        {

        }
        /// <summary>
        /// set custom repository
        /// </summary>
        /// <param name="_repository"></param>
        protected ReadService(INewsDataRepository<TEntity> _repository) : base(_repository)
        {
            IncludePropertyFunc = includeQuery => new TModel().IncludeProperties(includeQuery);
        }
        #endregion

        #region IREadService implementation
        public CollectionOperationResult<TModel> GetRecords(Expression<Func<TEntity, bool>> predicate)
        {
            try
            {
                var records = _NewsDataRepository
                    .Get(predicate, IncludePropertyFunc)
                    .ToModels<TModel, TEntity>();
                return CollectionSuccess(OperationTypes.Read, records.ToList());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return CollectionError(OperationTypes.Read, ServerMessages.CannotLoadRecords);
            }
        }

        public CollectionOperationResult<TModel> GetRecords()
        {
            return GetRecords(null);
        }

        public SingleOperationResult<TModel> GetSingleRecord(Expression<Func<TEntity, bool>> predicate)
        {

            try
            {
                var model = _NewsDataRepository.GetSingle(predicate, IncludePropertyFunc).ToModel<TModel, TEntity>();
                return SingleSuccess(OperationTypes.Read, model);
            }
            catch (Exception ex)
            {
                return SingleError(OperationTypes.Read);
            }
        }

        public virtual SingleOperationResult<TModel> GetSingleRecord(int id)
        {
            return GetSingleRecord(item => item.Id == id);
        }
        #endregion
    }
}
