﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedService.BLL.Services.BaseServices.ExtraClasses
{
    public static class ServerMessages
    {
        public static string UnsupportedOperation = "This operation operation is not supported";
        public static string TryAgain = "Please try again";
        public static string OperationCompleated = "Operation completed";
        public static string CannotPerformOperation = "Cannot perform this operation";
        public static string DataDownloaded = "Data has been successfully downloaded";
        public static string CannotLoadRecords = "Cannot load records";
        public static string ErrorInDataBase = "Error in database level";
        public static string ModelConversionError = "Error in while conversion to entity";
        public static string ErrorWhileRetrieving = "Error while retrieving new record from database";
    }
}
