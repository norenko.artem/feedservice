﻿using FeedService.BLL.Models;
using FeedService.BLL.Services.BaseServices.ExtraClasses;
using FeedService.BLL.Services.Interfaces;
using FeedService.DAL.Entity;
using FeedService.DAL.Options;

namespace FeedService.BLL.Services.Services
{
    public class NewsService : ReadWriteService<NewsModel, NewsItem>, INewsService
    {
        public NewsService(IDbContextOptionProvider optionsProvider) : base(optionsProvider)
        {

        }
        protected override NewsItem ConvertModelToEntity(OperationTypes operation, NewsModel model)
        {
            return base.ConvertModelToEntity(operation, model);
        }
        public override SingleOperationResult<NewsModel> GetSingleRecord(int id)
        {
            return GetSingleRecord(i => i.Id == id);
        }
    }
}
