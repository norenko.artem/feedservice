﻿using FeedService.BLL.Interfaces;
using FeedService.BLL.Services.BaseServices.ExtraClasses;
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedService.BLL.Services.Interfaces
{
    public interface IWriteService<TModel> : IDataService
         where TModel : class, IBaseModel, new()
    {
        /// <summary>
        /// Method for adding new entity into database
        /// </summary>
        /// <param name="model">Single model object to adding</param>
        SingleOperationResult<TModel> Add(TModel model);

        /// <summary>
        /// Method for adding range of new entities into database.
        /// </summary>
        /// <param name="models">List of models to add.</param>
        CollectionOperationResult<TModel> AddRange(IEnumerable<TModel> models);

        /// <summary>
        /// Method for updating entity in data base
        /// </summary>
        /// <param name="model">Single model object for updating</param>
        SingleOperationResult<TModel> Update(TModel model);

        /// <summary>
        /// Method for updating range of entities in data base
        /// </summary>
        /// <param name="models">Collection of models for updating</param>
        CollectionOperationResult<TModel> UpdateRange(IEnumerable<TModel> models);

        /// <summary>
        /// Method for removing record from database by entity id
        /// </summary>
        /// <param name="id">id of model to be deleted</param>
        SingleOperationResult<TModel> Remove(int id);

        /// <summary>
        /// Base method for update, delete and add operations for one model (type of operation is passed by 'operation' parameter)
        /// </summary>
        /// <param name="model">Full model to perform operation</param>
        /// <param name="operation">Operation type</param>
        /// <returns>Model response</returns>
        SingleOperationResult<TModel> SaveChanges(TModel model, OperationTypes operation);

        /// <summary>
        /// Base method for update, delete and add operations for collection of models (type of operation is passed by 'operation' parameter) 
        /// </summary>
        /// <param name="models">Models to save.</param>
        /// <param name="operation">Type of operation to be performed.</param>
        IEnumerable<SingleOperationResult<TModel>> SaveChanges(IEnumerable<TModel> models, OperationTypes operation);
    }
}
