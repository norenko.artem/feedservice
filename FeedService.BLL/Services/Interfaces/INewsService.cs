﻿using FeedService.BLL.Models;

namespace FeedService.BLL.Services.Interfaces
{
    public interface INewsService : IWriteService<NewsModel>, IReadService<NewsModel>
    {
    }
}
