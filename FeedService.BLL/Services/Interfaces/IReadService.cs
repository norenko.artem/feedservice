﻿using FeedService.BLL.Interfaces;
using FeedService.BLL.Services.BaseServices.ExtraClasses;
using FeedService.DAL.Entity;
using System;
using System.Linq.Expressions;

namespace FeedService.BLL.Services.Interfaces
{
    /// <summary>
	/// Contract for services for reading data from Database.
	/// </summary>
	public interface IReadService<TModel, TEntity> : IReadService<TModel>
        where TModel : class, IBaseModel, new()
        where TEntity : class, IBaseEntity, new()
    {
        /// <summary>
        /// Get one record by predicate
        /// </summary>
        /// <param name="predicate">Predicate</param>
        /// <returns>model object</returns>
        SingleOperationResult<TModel> GetSingleRecord(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Get all records by predicate
        /// </summary>
        /// <param name="predicate">Predicate</param>
        /// <returns>List of model objects</returns>
        CollectionOperationResult<TModel> GetRecords(Expression<Func<TEntity, bool>> predicate);
    }

    /// <summary>
    /// Contract for services for reading data from Database.
    /// </summary>
    public interface IReadService<TModel> : IDataService
        where TModel : class, IBaseModel, new()
    {
        /// <summary>
        /// Get one record by id
        /// </summary>
        /// <param name="id">Record id</param>
        /// <returns>model object</returns>
        SingleOperationResult<TModel> GetSingleRecord(int id);

        /// <summary>
        /// Get all records
        /// </summary>
        /// <returns>List of models</returns>
        CollectionOperationResult<TModel> GetRecords();
    }
}
