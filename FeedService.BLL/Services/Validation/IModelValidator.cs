﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FeedService.BLL.Services.Validation
{
    /// <summary>
    /// Contract for validation classes
    /// </summary>
    /// <typeparam name="TModel">Type of the model that should be validated</typeparam>
    public interface IModelValidator<TModel> where TModel : class, new()
    {
        IEnumerable<ValidationResult> Validate(TModel model);
    }
}
