﻿using FeedService.BLL.Interfaces;
using FeedService.DAL.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace FeedService.BLL.Models
{
    public class BaseModel : IBaseModel
    {
        public virtual int Id { get; set; }
        public virtual string Title { get; set; }
        public virtual string Description { get ; set ; }

        public virtual bool Validate(ICollection<ValidationResult> results = null)
        {
            var ctx = new ValidationContext(this, null, null);
            return Validator.TryValidateObject(this, ctx, results, true);
        }
        public virtual string GetFriendlyName()
        {
            return $"Record Id={Id}";
        }
    }

    /// <summary>
	/// Base class for models that are strongly tied to entity
	/// </summary>
	/// <typeparam name="TEntity"></typeparam>
	public abstract class BaseModel<TEntity> : BaseModel, IBaseModel<TEntity>
        where TEntity : IBaseEntity, new()
    {
        protected BaseModel()
        {
        }

        protected BaseModel(TEntity entity)
        {
            GetDataFromEntity(entity);
        }

        private void GetDataFromEntity(TEntity entity)
        {
            FromEntity(entity);
        }

        #region IBaseModel members

        public virtual IBaseModel FromEntity(TEntity entity)
        {
            Id = entity.Id;
            return this;
        }

        public virtual TEntity ToEntity()
        {
            return new TEntity { Id = Id };
        }
        public virtual IQueryable<TEntity> IncludeProperties(IQueryable<TEntity> includeQuery)
        {
            return includeQuery;
        }
        #endregion
    }
}
