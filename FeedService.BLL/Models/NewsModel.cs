﻿using FeedService.BLL.Interfaces;
using FeedService.DAL.Entity;

namespace FeedService.BLL.Models
{
    public class NewsModel : BaseModel<NewsItem>
    {
        public NewsModel()
        {

        }

        #region ModelData
        public string SourceUrl { get; set; }
        #endregion

        public override IBaseModel FromEntity(NewsItem entity)
        {
            Id = entity.Id;
            Title = entity.Title;
            Description = entity.Description;
            SourceUrl = entity.SourceUrl;
            return this;
        }
        public override NewsItem ToEntity()
        {
            var news = new NewsItem
            {
                Id = this.Id,
                Description = this.Description,
                Title = this.Title,
                SourceUrl= this.SourceUrl
            };
            return news;
        }
    }
}
