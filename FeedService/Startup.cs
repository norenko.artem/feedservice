﻿using FeedService.BLL.Services.Interfaces;
using FeedService.BLL.Services.Services;
using FeedService.DAL;
using FeedService.DAL.Options;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace FeedService
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        public Startup(IHostingEnvironment hostingEnvironment)
        {
            Configuration = new ConfigurationBuilder()
                           .SetBasePath(hostingEnvironment.ContentRootPath)
                           .AddJsonFile("appsettings.json")
                           .Build();
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddSingleton(Configuration);
            services.AddSingleton<IDbContextOptionProvider, DefaultDbContextOption>();
            services.AddDbContext<NewsContext>(
                options=>
                options.UseSqlServer(
                    Configuration.GetConnectionString("NewsConnection"),
                    b => b.MigrationsAssembly("FeedService.WebApp")
                    )
            );
            services.AddTransient<INewsService, NewsService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory LoggerFactory)
        {
            LoggerFactory.AddConsole();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler();
            }
            app.UseMvc(routes =>
            {

                //areas
                
                routes.MapRoute(
                name: "default",
                template: "{controller=HomeController}/{action=Home}");

                routes.MapRoute(
                    name: "news",
                    template: "{controller=NewsController}/{action=News}");

                routes.MapRoute(
                    name:"cabinet",
                    template:"{controller=UserAuth}/{action=Cabinet}");

            });
        }
    }
}
