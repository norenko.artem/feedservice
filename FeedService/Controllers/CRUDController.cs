﻿using FeedService.BLL.Interfaces;
using FeedService.BLL.Services.BaseServices.ExtraClasses;
using FeedService.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace FeedService.Controllers
{
    public abstract class RController<TModel, TService> : Controller
         where TModel : class, IBaseModel, new()
         where TService : IReadService<TModel>
    {
        protected readonly TService Service;

        /// <summary>
        /// Crates RController
        /// </summary>
        /// <param name="service"></param>
        protected RController(TService service)
        {
            Service = service;
        }

        #region Http R methods

        /// <summary>
        /// Get objects collection using current service
        /// </summary>
        /// <returns>Json that represent collection of object</returns>
        [HttpGet]
        public virtual ActionResult GetCollection()
        {
            var operationResult = Service.GetRecords();
            return Json(operationResult);
        }

        /// <summary>
        /// Getting one object from database using current service
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Json with success flag and message</returns>
        [Route("GetSingleObject")]
        [HttpGet("{id}")]
        public virtual ActionResult GetSingleObject(int id)
        {
            var operationResult = Service.GetSingleRecord(id);
            return Json(operationResult);
        }

        #endregion
    }

    /// <summary>
    /// Base controller to do CRUD operation with data base items
    /// </summary>
    /// <typeparam name="TModel">Type of model variant to data retrieving</typeparam>
    /// <typeparam name="TService">Service to read and write data</typeparam>
    public abstract class CRUDController<TModel, TService> : RController<TModel, TService>
        where TModel : class, IBaseModel, new()
        where TService : IWriteService<TModel>, IReadService<TModel>
    {

        #region Constructors

        /// <summary>
        /// Create CRUDController
        /// </summary>
        /// <param name="service"></param>
        protected CRUDController(TService service)
            : base(service)
        {
        }

        #endregion

        #region Http CUD methods

        /// <summary>
        /// Method to add new object to database using current service
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Json with success flag and message</returns>
        [HttpPost]
        public ActionResult AddObject([FromBody]TModel model)
        {
            var operationResult = PerformCRUDOperation(model, OperationTypes.Add);
            return Json(operationResult);
        }

        /// <summary>
        /// Delete object by id using current service
        /// </summary>
        /// <param name="id">Object id</param>
        /// <returns>Json with success flag and message</returns>
        [HttpDelete("{id}")]
        public ActionResult DeleteObject(int id)
        {
            var operationResult = PerformCRUDOperation(new TModel { Id = id }, OperationTypes.Delete);
            return Json(operationResult);
        }

        /// <summary>
        /// Update object by model using current service
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model">Model to update database record</param>
        /// <returns>Json with success flag and message</returns>
        [HttpPut("{id}")]
        public ActionResult UpdateObject(int id, [FromBody]TModel model)
        {
            model.Id = id;
            var operationResult = PerformCRUDOperation(model, OperationTypes.Update);
            return Json(operationResult);
        }

        private SingleOperationResult<TModel> PerformCRUDOperation(TModel model, OperationTypes operation)
        {
            BeforeStartCRUDOperation(model, operation);
            var operationResult = Service.SaveChanges(model, operation);
            CRUDOperationCompleted(operationResult.Record, operation, operationResult.WasSuccessful);
            return operationResult;
        }

        protected virtual void BeforeStartCRUDOperation(TModel model, OperationTypes operation)
        {
        }
        protected virtual void CRUDOperationCompleted(TModel model, OperationTypes operation, bool isSuccessful)
        {
        }

        #endregion
    }
}
