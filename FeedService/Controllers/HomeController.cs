﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FeedService.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace FeedService.WebApp.Controllers
{
    public class HomeController : Controller
    {
        [Route("")]
        [Route("Home")]
        public IActionResult Home()
        {
            return View();
        }
    }
}