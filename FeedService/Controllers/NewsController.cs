﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FeedService.BLL.Models;
using FeedService.BLL.Services.Interfaces;
using FeedService.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace FeedService.WebApp.Controllers
{
    [Route("News")]
    public class NewsController : CRUDController<NewsModel, INewsService>
    {
        private readonly INewsService _service;
        public NewsController(INewsService service):base(service)
        {
            _service = service;
        }
        public IActionResult NewsList()
        {
            return View();
        }
    }
}